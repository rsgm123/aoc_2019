import System.IO 
import System.Environment
import Data.List
import Data.List.Split
import Data.Array.IO
import Control.Monad
import Computer



main = do
    args <- getArgs
    file <- readFile $ head args
    let x = [8,5,9,7,6]
        r = runAmp file x x
    run' (r) [0]
--    print $ foldl max 0 $ map (\x -> head $ runAmp file x x [0]) (permutations [5..9])


run' r output = do
    let output' = r output
    print output'
    run' (r) output'


runAmp :: String -> [Int] -> [Int] -> [Int] -> [Int]
runAmp ampCode _ [] output = output
--runAmp ampCode phaseSettings [] previous = runAmp ampCode phaseSettings phaseSettings previous
runAmp ampCode phaseSettings (phase:xs) previous = runAmp ampCode phaseSettings xs previous'
    where previous' = runStringInput (getOp standardOps) ampCode ([phase]++previous)


