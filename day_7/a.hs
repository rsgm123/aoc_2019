import System.IO 
import System.Environment
import Data.List
import Data.List.Split
import Data.Array.IO
import Control.Monad
import Computer



main = do
    args <- getArgs
    file <- readFile $ head args
    print $ foldl max 0 $ map (\x -> head $ runAmp file x [0]) (permutations [0..4])


runAmp :: String -> [Int] -> [Int] -> [Int]
runAmp ampCode [] previous = previous
runAmp ampCode (phase:xs) previous = runAmp ampCode xs previous'
    where previous' = runStringInput (getOp standardOps) ampCode ([phase]++previous)

