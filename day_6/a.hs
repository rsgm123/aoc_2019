import Data.List.Split
import qualified Data.Set as Set
import qualified Data.Map.Strict as Map

main = do
    file <- getContents
    let input = lines file
    let input' = map ((\(a:b:_) -> (b, a)) . splitOn ")") input :: [(String, String)]
    let output = Map.foldl (\a b -> a + (length b)) 0 $ fn input' input' $ Map.singleton "COM" []
    print output


fn :: [(String, String)] -> [(String, String)] -> Map.Map String [String] -> Map.Map String [String]
fn _ [] orbits = orbits
--fn all ((a:"COM"):xs) orbits = fn all xs $ Map.insert a ["COM"] orbits
fn all ((a,b):xs) orbits = fn all xs $ Map.insert a route orbits'
    where
        orbits' = if Map.member b orbits
            then orbits
            else buildRoute all orbits b
        route = (orbits' Map.! b) ++ [b]


buildRoute :: [(String, String)] -> Map.Map String [String] -> String -> Map.Map String [String]
buildRoute all orbits b = Map.insert b route orbits'
    where
        c = lookup' $ lookup b all
        orbits' = if Map.member c orbits
            then orbits
            else buildRoute all orbits c
        route = (orbits' Map.! c) ++ [c]


lookup' :: Maybe String -> String
lookup' (Nothing) = "" -- should never happen
lookup' (Just b) = b

