import System.IO 
import System.Environment
import Data.List.Split
import Computer


main = do
    args <- getArgs
    file <- readFile $ head args
    runFromString (getOp standardOps False) file

