import System.IO 
import System.Environment
import Data.List.Split
import Computer


debug = False

main = do
    args <- getArgs
    file <- readFile $ head args
    print $ runString (getOp standardOps) file

