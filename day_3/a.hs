import Data.List.Split
import qualified Data.Set as Set

main = do
    file <- getContents
    print . minDistance . crossings . map ((points (0,0)) . (map (directions)) . (splitOn ",")) $ lines file


data Direction = U | D | R | L deriving (Read)


directions :: [Char] -> (Direction, Int)
directions (x:xs) = (read [x], read xs)

points :: (Int, Int) -> [(Direction, Int)] -> [(Int, Int)]
points (x,y) ((direction, distance):xs) = points' ++ points (last points') xs
    where points' = case direction of
            U -> [(x + i,y) | i <- [1..distance]]
            D -> [(x - i,y) | i <- [1..distance]]
            R -> [(x,y + i) | i <- [1..distance]]
            L -> [(x,y - i) | i <- [1..distance]]
points (x,y) [] = []

crossings :: [[(Int, Int)]] -> Set.Set (Int, Int)
--crossings (xs1:xs2:_) = [x | x <- xs1, x `elem` xs2]
crossings (xs1:xs2:_) = xs1' `Set.intersection` xs2'
    where
        xs1' = Set.fromList xs1
        xs2' = Set.fromList xs2

minDistance :: Set.Set (Int, Int) -> Int
minDistance xs = minimum $ Set.map (\(x,y) -> (abs x) + (abs y)) xs

