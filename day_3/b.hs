import Data.List.Split
import qualified Data.Set as Set

main = do
    file <- getContents
    print . run $ lines file


run :: [[Char]] -> Int
run xss = minimum . map (stepsToPoint points') . crossings $ points'
    where points' = map ((points (0,0)) . (map (directions)) . (splitOn ",")) xss



data Direction = U | D | R | L deriving (Read)


directions :: [Char] -> (Direction, Int)
directions (x:xs) = (read [x], read xs)

points :: (Int, Int) -> [(Direction, Int)] -> [(Int, Int)]
points (x,y) ((direction, distance):xs) = points' ++ points (last points') xs
    where points' = case direction of
            U -> [(x + i,y) | i <- [1..distance]]
            D -> [(x - i,y) | i <- [1..distance]]
            R -> [(x,y + i) | i <- [1..distance]]
            L -> [(x,y - i) | i <- [1..distance]]
points (x,y) [] = []

crossings :: [[(Int, Int)]] -> [(Int, Int)]
crossings (xs1:xs2:_) = Set.toList $ xs1' `Set.intersection` xs2'
    where
        xs1' = Set.fromList xs1
        xs2' = Set.fromList xs2

stepsToPoint :: [[(Int, Int)]] -> (Int, Int) -> Int
stepsToPoint (xs1:xs2:_) point = l1 + l2 + 2
    where
        l1 = length . takeWhile (/= point) $ xs1
        l2 = length . takeWhile (/= point) $ xs2

