module Computer
( runString
, runStringInput
, parseProgram
, run
, getOp
, standardOps
) where


import System.Environment
import Data.List.Split


data Mode = R | W deriving Eq
data State = State { memory :: [Int]
                   , pc     :: Int
                   , rb     :: Int
                   , input  :: [Int]
                   , output :: [Int]
                   }
type RawParams = [(Int, Int)]
type ParamFn = ([Mode] -> [Int])
type Op = (ParamFn -> State -> State)


-- main = do
--     args <- getArgs
--     file <- readFile $ head args
--     runFromString (standardOps False) file


runString :: (Int -> Op) -> String -> [Int]
runString getOpFn program = runStringInput getOpFn program []


runStringInput :: (Int -> Op) -> String -> [Int] -> [Int]
runStringInput getOpFn program input = run (getOpFn) (State ((parseProgram program) ++ (repeat 0)) 0 0 input [])


parseProgram :: String -> [Int]
parseProgram xs = map read $ splitOn "," xs :: [Int]


run :: (Int -> Op) -> State -> [Int]
run _ (State (99:_) 0 _ _ output) = output
run getOpFn state = run (getOpFn) state'
    where (opCode, params) = parseOp state -- parse opcode
          op = getOpFn opCode              -- get operation
          state' = op (params) state       -- run operation


memoryPc :: [Int] -> Int -> [Int]
memoryPc memory pc = snd $ splitAt pc memory


standardOps :: [(Int, Op)]
standardOps = [ (1, addMult (+))
              , (2, addMult (*))
              , (3, inputOp)
              , (4, outputOp)
              , (5, jumpIf (/=0))
              , (6, jumpIf (==0))
              , (7, addMult (\a b -> if a < b then 1 else 0))
              , (8, addMult (\a b -> if a == b then 1 else 0))
              , (9, rbAdd)
              , (99, (\_ (State _ _ _ _ output) -> State [99] 0 0 [] output))
              ]


getOp :: [(Int, Op)] -> Int -> Op
getOp ops opCode = op
    where maybeOp = lookup opCode ops
          op = case maybeOp of
              (Just op') -> op'
              Nothing    -> (\params state -> (State [99] 0 0 [] [-1, opCode]))


parseOp :: State -> (Int, ParamFn)
parseOp state@(State memory pc _ _ _) = (op, paramFn)
    where (opCode:program) = memoryPc memory pc
          op = opCode `mod` 100
          c = ((opCode `div` 100) `mod` 10)
          b = ((opCode `div` 1000) `mod` 10)
          a = ((opCode `div` 10000) `mod` 10)
          paramFn = getParams state $ zip [c, b, a] program


getParams :: State -> RawParams -> ParamFn
getParams (State memory _ rb _ _) params modes = map getParam $ zip modes params
    where getParam (mode, (value, param)) = if mode == W
              then case value of
                  2 -> param + rb
                  _ -> param
              else case value of
                  0 -> memory !! param
                  1 -> param
                  2 -> memory !! (param + rb)


setAddress :: (Int, Int) -> [Int] -> [Int]
setAddress (address, value) memory = memA ++ [value] ++ tail memB
    where (memA, memB) = splitAt address memory


-- ops

addMult :: (Int -> Int -> Int) -> Op
addMult op params (State memory pc rb input output) = State memory' (pc + 4) rb input output
    where (c:b:a:_) = params [R, R, W]
          result = c `op` b
          memory' = setAddress (a, result) memory

inputOp :: Op
inputOp _ (State memory pc rb [] output) = State memory (pc + 2) rb [] output
inputOp params (State memory pc rb input output) = State memory' (pc + 2) rb input' output
    where (c:_) = params [W]
          (i:input') = input
          memory' = setAddress (c, i) memory

outputOp :: Op
outputOp params (State memory pc rb input output) = State memory (pc + 2) rb input output'
    where (c:_) = params [R]
          output' = output ++ [c]

jumpIf :: (Int -> Bool) -> Op
jumpIf op params (State memory pc rb input output) = State memory pc' rb input output
    where (c:b:_) = params [R, R]
          pc' = if op c then b else pc + 3

rbAdd :: Op
rbAdd params (State memory pc rb input output) = State memory (pc + 2) rb' input output
    where (c:_) = params [R]
          rb' = rb + c

