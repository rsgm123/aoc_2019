import Control.Monad

main = do
    file <- getContents
    putStr $ show $ foldl (\x y -> x + moduleReq (read y :: Int)) 0 (lines file)


moduleReq :: Int -> Int
moduleReq x = sum $ moduleReqs x


moduleReqs :: Int -> [Int]
moduleReqs x
    | x' > 0 = [x'] ++ moduleReqs x'
    | otherwise = []
    where x' = x `div` 3 - 2

