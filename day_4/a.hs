import Data.List.Split
import Control.Applicative

main = do
    -- file <- getContents
    -- let input = lines file
    let input = map read $ splitOn "-" "367479-893698" :: [Int]
    let output = length . filter (and . ((. splitNumber) <$> [notDecreasing, doubles] <*>) . pure) $ [(head input)..(last input)]
    print output


doubles :: [Int] -> Bool
doubles xs = (>=1) . length . filter (== 2) . map length $ foldl doubleFold [[]] xs

doubleFold :: [[Int]] -> Int -> [[Int]]
doubleFold [[]] x = [[x]]
doubleFold xs x
    | (last $ last xs) == x = init xs ++ [(last xs ++ [x])]
    | otherwise = xs ++ [[x]]

notDecreasing :: [Int] -> Bool
notDecreasing (x:[]) = True
notDecreasing (x:xs) = x <= head xs && notDecreasing xs

splitNumber :: Int -> [Int]
splitNumber x = [(x `div` 10^y) `mod` 10 | y <- [length,length-1..0]]
    where length = floor $ logBase 10 (fromIntegral x :: Double)

