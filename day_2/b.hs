import Data.List.Split


main = do
    file <- getContents
    print . findAnswer . map read $ splitOn "," file

answer = 19690720

inputs =concat $ map concat [[[(y,x),(x,y)] | y <- [1..x]] | x <- [1..]]

fixInput :: (Int, Int) -> [Int] -> [Int]
fixInput (x,y) (a:_:_:xs) = [a, x, y] ++ xs 

findAnswer :: [Int] -> Int
findAnswer xs = calc a
    where
        results = map (\x -> (head . run 0 $ fixInput x xs, x)) inputs
        a = head $ dropWhile ((/= answer) . fst) results
        calc (_,(x,y)) = 100 * x + y

run :: Int -> [Int] -> [Int]
run i xs
    | head op == 99 = xs
    | otherwise = run (i+1) (runOp op xs)
        where op = getOp i xs


getOp :: Int -> [Int] -> [Int]
getOp i xs = snd (splitAt (i*4) xs)


runOp :: [Int] -> [Int] -> [Int]
runOp (o:a:b:r:_) xs = setResult (r, result) xs
    where
        op = if o == 1 then (+) else (*)
        result = (xs!!a) `op` (xs!!b)


setResult :: (Int, Int) -> [Int] -> [Int]
setResult (x, r) xs = fst s ++ [r] ++ tail (snd s)
    where s = splitAt x xs

