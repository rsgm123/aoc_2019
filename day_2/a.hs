import Data.List.Split


main = do
    file <- getContents
    print . head $ run 0 $ fixInput . map read $ splitOn "," file
    

fixInput :: [Int] -> [Int]
fixInput (a:_:_:xs) = [a, 12, 2] ++ xs 


run :: Int -> [Int] -> [Int]
run i xs
    | head op == 99 = xs
    | otherwise = run (i+1) (runOp op xs)
        where op = getOp i xs


getOp :: Int -> [Int] -> [Int]
getOp i xs = snd (splitAt (i*4) xs)


runOp :: [Int] -> [Int] -> [Int]
runOp (o:a:b:r:_) xs = setResult (r, result) xs
    where
        op = if o == 1 then (+) else (*)
        result = (xs!!a) `op` (xs!!b)


setResult :: (Int, Int) -> [Int] -> [Int]
setResult (x, r) xs = fst s ++ [r] ++ tail (snd s)
    where s = splitAt x xs

