import Data.List.Split

main = do
    file <- getContents
    let input = init file -- removes trailing '\n'
        layers = splitter layerSize $ input
        o = foldl1 (z) layers
    putStrLn $ unlines $ splitter rowSize o


rowSize = 25
layerSize = rowSize * 6


z :: [Char] -> [Char] -> [Char]
z a b = map (\(a', b') -> if a' == '2' then b' else a') xs
    where xs = zip a b


splitter :: Int -> [a] -> [[a]]
splitter _ [] = []
splitter size xs = [c] ++ (splitter size d)
    where (c, d) = splitAt size xs

