import Data.List.Split

main = do
    file <- getContents
    let input = file
        -- input' = map (\x -> read [x]) file :: [Int]
        layers = filter ((>3) . length) $ toLayers $ input
        layers' = map (\layer -> (countDigits '0' layer, (countDigits '1' layer) * (countDigits '2' layer))) layers
        m = minimum . map (\(x,_) -> x) $ layers'
        (_,o) = head $ filter (\(x,_) -> x == m) layers'
    print $ m
    print $ o
    print $ filter ((== 0) . countDigits '0') layers
-- 

layerSize = 25 * 6

countDigits :: Char -> [Char] -> Int
countDigits d xs = length (filter (== d) xs)

toLayers :: [Char] -> [[Char]]
toLayers [] = [[]]
toLayers xs = [c] ++ (toLayers d)
    where (c, d) = splitAt layerSize xs
